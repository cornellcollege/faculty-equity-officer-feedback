// This next 2 lines set the base URL of the application into the JavaScripting.
let wonkyPath = window.location.pathname.indexOf('index.php');
// WWW application 
let baseUrl = wonkyPath != -1? "https://www.cornellcollege.edu"+window.location.pathname.substr(0, wonkyPath) : "https://www.cornellcollege.edu"+window.location.pathname;
let ajaxbaseUrl = wonkyPath != -1? "https://www.cornellcollege.edu"+window.location.pathname.substr(0, wonkyPath) : "https://www.cornellcollege.edu"+window.location.pathname;
// secure application
//let baseUrl = wonkyPath != -1? "https://secure.cornellcollege.edu"+window.location.pathname.substr(0, wonkyPath) : "https://secure.cornellcollege.edu"+window.location.pathname;
//let ajaxbaseUrl = wonkyPath != -1? "https://secure.cornellcollege.edu"+window.location.pathname.substr(0, wonkyPath) : "https://secure.cornellcollege.edu"+window.location.pathname;

// Should we use the date picker program
// Possible values: none, dateTime & dateOnly
var datePickerType = 'none';

// appName is used in the file '/assets/production/js/perappjs.js' which contains centralize source code.
// If the page does not allow the '$' for JQuery, this is where you can define JQuery functions that associated by appName
var appName = 'equityFeedbackApp';

// A global variable for the max word count
let iWordCountLimit = 500;

// variables fro displaying error messages
let bullet = '\u2022' + ' ';
let sErrorMessage = '';

// validation variables
let bSponsorNameComplete = true;
let bBeenThereComplete = true;

// Array of field name to max string length
let aMaxStringLength = {
    requestor : 50,
    room : 50,
    dept : 30,
    phone4 : 15,
    location : 50,
    username : 50,
    iIsBmanager : 15,
    iIsFacmanager : 50,
    iIsAdmin : 30,
};

////////////////////////////////////////////////
// some standard js
///////////////////////////////////////////////
/*
 * function showdiv
 * This function changes the display to visible for the given element
 * @param string, {Id}, the name of the selection element to test.
*/
function showdiv(id){
    $("[id='"+id+"']").show();
}

/*
 * function hidediv
 * This function changes the display to none for the given element
 * @param string, {Id}, the name of the selection element to test.
*/
function hidediv(id){
    $("[id='"+id+"']").hide();
}

/*
 * function hideOnClick
 * This function hides the given element 
*/
function hide_OnClick(elementId){
    $("#"+elementId).addClass('is-hidden');
}

///////////////////////////////////////////////
// an on ready script
//////////////////////////////////////////////
// I will be using the ready trigger to set up the button on click events for my examples
try{
	$(document).ready(function(){
        $('#overlay').addClass('is-hidden');
        $('input').blur(function(){
            // So we are here
            let sTheId = $(this).attr('id');
            if((typeof aMaxStringLength[sTheId] != 'undefined') && (aMaxStringLength[sTheId] > 0)){
                bTestThatStringIsOK(sTheId,aMaxStringLength[sTheId]);
            }
        });
        // Add onClick calls to buttons
        $('button#logout').click(function() {
            let Message =  "Log Out!";
            let Title = "Are you sure";
            let ConfirmationMessage = "Continue";
            if(typeof showconfirmationMessage == 'function'){
              showconfirmationMessage(Title,Message,ConfirmationMessage);
            } else {
              window.location.replace(baseUrl+"/index.php/logout");
            }
        });
        // Add require property to the field(S)
//        $('#rygpg').attr('required', true);
        // The text area fiedl has changed so test if it excites the allowed number of words.
        //<TextAreaFieldNameToCheck>
        $('#feedback_text').change(function(){
            let aStringInfo = wordCount($('#feedback_text').val());
            //<TextAreaFieldNameToCheck>
            $('#feedback_text').removeClass('error');
            if(aStringInfo.words > iWordCountLimit){
                let title = "Oops...";
                let sMessage =  "Your response may only be " + iWordCountLimit + " words long!";
                //<TextAreaFieldNameToCheck>
                $('#feedback_text').addClass('error');
                showInformationalMessage(title,sMessage,'','error',true,'returnBtn6',"$('#returnBtn6').html('Error Confirmed');");                
            }
        });
    });
    
} catch (e) {
	console.log(e.message);    //this executes if jQuery isn't loaded
}

/**
 * wordCount function used to find how many words are in a text field
 * @param {String} val String to count the words
 * @returns {Array} An array of information about the given string; charactersNoSpaces, characters, words, lines
*/
function wordCount( val ){
    let wom = val.match(/\S+/g);
    return {
        charactersNoSpaces : val.replace(/\s+/g, '').length,
        characters         : val.length,
        words              : wom ? wom.length : 0,
        lines              : val.split(/\r*\n/).length
    };
}

function sReflectTheInput(){
    showInformationalMessage("Sweet!","You wrote: " + $('#returnBtn7').html(),"","success");
}
//checks that the required fields are completed
function bAllRequiredSet(){
  $('#overlay').removeClass('is-hidden');
  //<FieldNameToCheck>
  if($('#contact').val() > 0){
    bContactComplete = true;
  } else {
    bContactComplete = false;
    sErrorMessage = sErrorMessage + bullet + 'You have not yet selected whether or not you would like to be contacted, this field is located at the bottom of this form.' + '\r\n';
  }
  if(validateEmail($('#email').val()) && $('#contact').val() == 2){
    bEmailCorrect = true;
  }else if($('#contact').val() == 2) {
    bContactComplete = false;
    sErrorMessage = sErrorMessage + bullet + 'The email entered is not valid' + '\r\n';
  }else if($('#contact').val() == 1){
    bEmailCorrect = true;
  }
  if(bContactComplete){
    // required field has been filled in show submit button
    $('#checkButton').addClass('is-hidden');
    $('#submitButton').removeClass('is-hidden');
    $('#overlay').addClass('is-hidden');
  } else {
    // User hasn't completed the required fields this is an error, show alert
    $('#submitButton').addClass('is-hidden');
    $('#checkButton').removeClass('is-hidden');
    showInformationalMessage('Selection Error: ', sErrorMessage, '', 'error');
    $('#overlay').addClass('is-hidden');
    sErrorMessage = '';
  }
}

function bShowContact(){
  //<FieldNameToCheck>
  if($('#contact').val() > 0){
    bContactComplete = true;
  } else {
    bContactComplete = false;
  }
  if(bContactComplete){
    // required field has been filled in show submit button
    $('#contactInfo').removeClass('is-hidden');
    $('#email').addClass('required');
    $('#phone_num').addClass('required');
  }
}

// Generic function to test if a give field is an email address
function bIsEmail(sFieldId){
  let email = $("#"+sFieldId).val();
  if (!validateEmail(email)) {
    alert(email + " is not a valid email address");
    $("#"+sFieldId).val('');
    document.getElementById("sFieldId").focus();
  }
  return false;
}

$("#internship_site_email").blur(function(e){
   let sEmail = $("#internship_site_email").val();
   if(validateEmail(sEmail) === false){
      alert('Invalid internship site email address.');
      e.preventDefault();
      return false;
   } else {
      alert('Email OK');
   }
});

// Function to test if the entered eMail address matches the 5322 standard
function validateEmail(sEmail){
    let filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!filter.test(sEmail)){
        return false;
    } else {
        return true;
    }
}

function bHideSubmitButton(){
  $('#overlay').removeClass('is-hidden');
  $('#submitButton').addClass('is-hidden');
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////AJAX Stuff ////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajax vars
let nonedit = baseUrl+'ajax.php/nonedit';

// Here is an example of using the ajax functionality
// This function is looking for an array of arrays to come back:
//  $aReturnArry =
//     ['idx' or 0] => array( // also known as obj)
//                   'name' => 'Dan',
//                   'value' => 'zippy',
//                   'time' => '2020:04:02 20:10:00'
//            );
function getNonEditables(){
   $.getJSON(nonedit, null, function(data){
      $.each(data, function(idx, obj){
         $msgInfo = obj.name + ' ' + obj.value + ' ' + obj.time;
         alert($msgInfo);
      });
   });
}

// Auto Complete code uses //code.jquery.com/ui/1.11.0/jquery-ui.js & //code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css
$().ready(function() {
	if($('.autocomplete.users input').length > 0) {
		$('.autocomplete.users input').autocomplete({
			source: baseUrl + 'ajax.php/autocomplete_users',
			minLength: 2
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
			.append( item.label )
			.appendTo( ul );
		};
	}
});