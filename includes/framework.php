<?php
namespace CLib;

/**
 * CornellFramework class extends the namespace Framework class
 * @property SQL $sql if using either mysql (being retired) or mysqlPDO
 * @property SQL $sungard if using mssql
 * @property class Form auto loaded 
 * @property method include_css the include method from core framework
 * @property function sBuildBanner builds the HTML wrapper for the banner
 * @property function set passes the named variable of the given value into the display page
 * @property function redirect redirects the application to the given URL
 * @property function is_admin checks if the user is in the admins array of the config file
*/

class CornellFramework extends Framework {
    // Variables used in the class
	// Page banner string
	public $sBanner;
	// Data handler object
	protected $data;
	// for standard input
	// This variable is used for capturing the $_GET data being passed in.
	public $aGetArray = array();
	public $isAdmin;
	// This is the default input form name
	private $sFormName = 'equity_officer_feedback_form';

    /**
     * class constructor currently no parameters are being passed in.
     * Add any special construction processing here.
     * One of the practices is to run the parent construct to make sure the base Framework is built correctly
    */
	public function __construct(){
		parent::__construct();
	}

	/**
     * class initialization function
     * Add in processing and application settings here.
     * One of the practices is to run the parent init to make sure the base Framework is initialized correctly
     * @param array $config The application  configuration array.
    */
    public function init($config){
		// Run the parent initialization
		parent::init($config);
		// Instantiate the data handler
		require_once('includes/datahandler.php');
		$this->data = new CornellDataHandler($this, $this->sql, $config, $this->dynamicPC, $this->dynamicGP, true, true, $this->dynamicPF);
		// Get the user data.
		$this->user = $this->user_gets($this->get_username());
		$this->sMessage = '';
		// Is this username in the select security group.
		if($this->is_admin() or $this->isMemberOfs($user,$this->$config['sPermissionTeam'])){
			$this->isAdmin = True;
		} else {
			$this->isAdmin = False;
		}
	}
	
    /**
     * before_filter function. This function is used to run any work needed prior to any other action being run.
     * This function is run during the route operation in the Framework the only things that happen prior to this
     * are:
     * 	Test if the request was to logout,
     * 	Test if this is an impersonate,
     * 	and set up the naming for AJAX calls
     * @param NONE
    */
	public function before_filter(){
		// Read in the $_GET (get query) passed in if any. This is a pre-submit thing.  If you
        // use this then make sure to set any form you may have on the page to use the post method for it's action
        if(isset($_GET)){
			if(is_array($_GET)){
				foreach($_GET as $key=>$val){
					if($key == 'term'){
						// This variable is used in the autocomplete process
						$this->sTerm = $_GET['term'];
					}
					$this->aGetArray[$key] = $val;
				}
			}
        }
		// Clear out the $_GET system variable to reduce code injection issues
        if(isset($_GET)){
            unset($_GET);
        }
	}

    /**
     * before render function. This function is used to run any work needed prior to rendering the page.
     * This function is run prior to showing the page
     * @param NONE
    */
	public function before_render(){
        // include the application specific style sheet located in <ApplicationRoot>/css
		$this->include_css('style');
        // include the application specific JavaScript located in <ApplicationRoot>/js
        $this->include_js_in_tail('app');
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// PAGES ///////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * index function. This is the main landing page for the application
     * Currently there are no parameters being passed in.
     * @param NONE
     * @return Object The page displayed
    */
	public function index(){
		$this->testingRedirect();
		$form = new Form('feedbackform');
        // Did the user submit and did it pass the validation process?
		if($form->validate()){
            // Yes this is validate input
            // Read in the input values both hidden and user input
			$theFormsValues = $form->getValue();
			$emailMessage = $this->data->insertEquityOfficerFeedback($theFormsValues);
			if($emailMessage != null){
				$email = $this->config['admins'] .'@cornellcollege.edu';
				$this->sendEmail("New Equity Officer Feedback Form", $emailMessage, $email);
				$this->redirect('/thanks');
			}else{
				$this->redirect('/error');	
			}
		}
		$this->set('form', $form);
        // Set the banner for the page.
		//<REPLACE with Application Form Name>
		$this->sBanner = $this->sBuildBanner('Welcome to the Equity Officer Feedback Form');
        // If there is a sMessage set send it along to the page display.
        // This can be an informational or error sMessage.
		$bWorked = $this->buildMessage();
		$this->set('message',$this->sMessage);
        // Set the banner for the display page.
		$this->set('banner',$this->sBanner);
        // We will pass along the user information to the index page
        $this->set('user',$this->user);
		// Pass in the other variables
		$this->set('aSelectChoices',$this->data->aSelectChoices);
	}

    /**
     * admin function. This is the function which will be used to update the
     * information to display on the screen
     * Currently there are no parameters being passed in.
     * @param NONE
     * @return Object The page displayed
    */
	public function admin(){
		if($this->isAdmin){
			$data = $this->data->getFeedback();
			$this->set('results',$data);
		}else{
			$this->redirect('/notauth');
		}
	}
	/**
	 * Thanks function, displays thanks page.
	 */
	public function thanks(){

	}
	/**
	 * Not Authorized. 
	 */
	public function notauth(){

	}
	/**
	 * Feedback error page. 
	 */
	public function error(){

	}
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////// Common functions /////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * buildMessage is used to build in the error messages for a page
	 * @param NONE
	 * @sets String $this->sMessage The message to pass into the page usually an error message
	 * @return true
	*/
	public function buildMessage(){
		if(strlen($this->data->sErrorMessage) > 0 ){
			$this->sMessage .= '<br />' . $this->data->sErrorMessage . '<br />' . $this->sBaseHelpDeskMessage;
		} elseif (strlen($this->sMessage) > 0 ){
			$this->sMessage .= '<br />' . $this->sBaseHelpDeskMessage;
		}
		return true;
	}
	public function testingRedirect(){
		if($this->config['testing']) {
			$this->isTesting = True;
            if((! $this->is_admin())) {
				// Redirect to the testing page
                $this->redirect('/testing');
			}else {
				echo 'In Testing Mode!!! <br />';
            }
		}
	}
    /**
	 * function to send email
	 * @param subject the subject of the email
	 * @param body the body of the email to send
	 * @param email the email it is sent to
	 * @param from the email it is sent from
	 * @param html whether html was used in the body of the email
	*/
	public function sendEmail($subject, $body, $email, $from = 'no-reply@cornellcollege.edu', $html = false){
        if ($this->config['debug'] || $this->config['testing']) {
			// Direct all emails to application admin
			$subject = $email . ':' . $subject;
			$email = $this->config['admins'][0] .'@cornellcollege.edu';
			// call the Framework (black box) send email
			return $this->send_email($subject, $body, $email, $from, $html);
		} else {
			// call the Framework (black box) send email
			return $this->send_email($subject, $body, $email, $from, $html);
		}
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// AJAX ////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * ajax_query retrieves the files names
     * Ajax call from JS
    */
    function ajax_nonedit() {
        $query = $this->data->check_list_sponsers($_SESSION['username']);
		// AXJAX call is looking for a return of an array of arrays
        echo json_encode($query);
        sleep(1);
		exit;
    }

    /**
     * ajax_autocomplete_users used to retrieve name listing for auto completion
     * @param none
     * @return json encoded string
     * ajax call from JS
    */
	public function ajax_autocomplete_users(){
		$results = array();
		if(strlen($this->sTerm) > 2) {
			$results = $this->users_gets($this->sTerm);
		}
		$user = array();
		foreach($results as $user) {
            $user[] = array(
                'label' => $user['name'],
                'desc' => $user['id'],
                'value' => $user['username']
                );
		}
		$this->render_json($user);
	}

}
