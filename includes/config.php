<?php
/*
This is the configuration file for the application.
It is used to set the basic information needs and constants
for the application.  This file is local to the includes
directory until the application has been tested and moved
into production.  Once in production this file will be
moved to the central configuration directory and the index.php
file will be undated.
*/

/*
First set up the connection information for any and all databases
needed by the application.
*/
// Configures the connection to the database that will be used for this application.
// uses PHP PDO and is a MySQL database (InnoDB engine)
$config['db'][] = array(
	'name'   => "sql",
	'dbName' => "it_sp_test",
	'dbHost' => "mysql.cornellcollege.edu",
	'dbUser' => "webteam",
	'dbType' => "mysqlPDO",
);
// configuration of the Power Campus database is required by Datahandler, webteam is the best connector
$config['db'][] = array(
	'name'   => "dynamicPC",
	'dbName' => "campus6",
	'dbHost' => "pcdb",
	'dbUser' => "webteam",
	'dbType' => "mssql",
	);
// configuration of the Great Plains database is required by Datahandler, webteam is the best connector
$config['db'][] = array(
	'name'   => "dynamicGP",
	'dbName' => "CORN",
	'dbHost' => "dyndb",
	'dbUser' => "webteam",
	'dbType' => "mssql",
	);
// configuration of the Power FAIDS database is required by Datahandler, webteam is the best connector
$config['db'][] = array(
	'name'   => "dynamicFP",
	'dbName' => "PowerFAIDS",
	'dbHost' => "pcdb",
	'dbUser' => "webteam",
	'dbType' => "mssql",
);

/*
This section lists main application page data and (global) constants;
things like title, administrators, login required, custom login, debug status
*/
// The default page title, shows in the browser tab. The favicon is the standard Cornell College icon
//<ApplicationTitle>
$config['title'] = "Faculty Equity Officer Feedback Form";
// Require the user to log in?
$config['login_required'] = true;
// are we in debug mode?
$config['debug'] = false;
//$config['debug'] = true;
// Are we in test mode?
$config['testing'] = false;
$config['testing'] = true;
// Setting the admins for the page
//$config['admins'][] = 'ntatum';
//$config['admins'][] = 'dhakken';
//$config['admins'][] = 'eflanagan';
//$config['admins'][] = 'ayates';
$config['admins'][] = 'myamanishi';
$config['admins'][] = 'hcollier';
$config['admins'][] = 'sbray';

// The special permissions group this is used in the Framework isMemberOfs to tell if this user has special permissions
//$config['sPermissionTeam'] = '';

//***************************************************************************
//To put app in mode of testing as student:
//Un/comment "isTestingAsStudent = true' below
//***************************************************************************
$config['isTestingAsStudent'] = false;
//$config['isTestingAsStudent'] = true;

//***************************************************************************
//To put app in mode of testing as professor/Registrar:
//Un/comment "isTestingAsStudent = true' below
//***************************************************************************
$config['isTestingAsProf'] = false;
//$config['isTestingAsProf'] = true;

// The contact phone number IT office in this case
$config['contactPhoneNumber'] = '4357';

// Who should receive email notifications? Or use the email table structure.
//$config['adminEmail'][] = 'dhakken@cornellcollege.edu';
//$config['adminEmail'][] = 'webmaster@cornellcollege.edu';

// why custom? setting $this->config['login_required'] on certain pages (such as /show) still required login.
// with custom login enabled, all pages require login except /show and /404 etc. 
// set to false if you want to remove login requirement.
$config['custom_login_required'] = false;

/*
This section is used to specify any additional things (files, metadata, styles)
to include in the <head> tag area for this application which are not already
being read in by the page layout file (listed later).
*/
$config['head'] = '
    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
';
/*
This section is used to specify any additional things (files, metadata, styles)
to include at the end of the <body> tag area for this application which are
not already being read in by the page layout file (listed later).
*/
$config['tail'] = '
	<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
';

// If you need to go to a directory other than CLIB/templates use the next line
//$config['page_layout_dir'] = '';

// Setting the page's layout
//$config['page_layout'] = 'viking-100-percent';
$config['page_layout'] = 'viking';

?>
