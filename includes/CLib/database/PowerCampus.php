<?php

/**
 * PowerCampus
 *
 * @version 0.2.1
*/

namespace CLib\Database;

class PowerCampus{
    public function __construct($dynamicPC){
        $this->dynamicPC = $dynamicPC;

        $this->columnsToGet = array();
    }

    public function columns(array $columns){
        $this->columnsToGet = $columns;

        return $this;
    }

    public function fillInUsers($records, $userReferences = 'user'){
        if(!is_array($userReferences)){
            $userReferences = array($userReferences);
        }

        $userIDs = $this->findAllUserIDs($records, $userReferences);

        $inQuerySegment = $this->buildInQuerySegment($userIDs);
        $completeQuery = $this->buildCompleteQuery($inQuerySegment);
    
        $userResults = $this->dynamicPC->query($completeQuery);

        $users = array();

        foreach($userResults as $userResult){
            $users[$userResult['id']] = $userResult;
        }
        
        $records = $this->addToRecords($records, $users, $userReferences);

        $this->resetQuery();

        return $records;
    }

    public function addToRecords($records, $users, $userReferences){
        foreach($records as $key => $record){
            foreach($userReferences as $userReference){
                if(isset($record[$userReference.'_id'])){
                    $records[$key][$userReference] = $users[$record[$userReference.'_id']];
                }
            }
        }

        return $records;
    }

    public function buildCompleteQuery($inQuerySegment){
        $completeQuery = "SELECT PEOPLE.PEOPLE_ID AS id, ";

        if(in_array('name', $this->columnsToGet)){
            $completeQuery .= "CONCAT(CASE WHEN LEN(NICKNAME) > 0 THEN NICKNAME ELSE FIRST_NAME END, ' ', LAST_NAME) AS name, ";
        }

        if(in_array('email', $this->columnsToGet)){
            $completeQuery .= "EMAILADDRESS.email AS email, ";
        }

        // Remove the last comma.
        $completeQuery = substr($completeQuery, 0, -2);

        $completeQuery .= " FROM PEOPLE ";

        if(in_array('email', $this->columnsToGet)){
            $completeQuery .= "LEFT JOIN EMAILADDRESS ON EMAILADDRESS.EmailAddressId = PEOPLE.PrimaryEmailId ";
        }

        $completeQuery .= "WHERE PEOPLE.PEOPLE_CODE_ID IN ".$inQuerySegment.";";  

        return $completeQuery;
    }

    public function findAllUserIDs($records, $userReferences){
        $userIDs = array();

        foreach($records as $record){
            foreach($userReferences as $userReference){
                if(isset($record[$userReference.'_id']) && !in_array($record[$userReference.'_id'], $userIDs)){
                    $userIDs[] = $record[$userReference.'_id'];
                }
            }
        }

        return $userIDs;
    }

    public function buildInQuerySegment($userIDs){
        $inQuery = '(';

        foreach($userIDs as $userID){
            $inQuery .= "'P".trim($userID)."', ";
        }

        $inQuery = substr($inQuery, 0, -2);

        $inQuery .= ")";

        return $inQuery;        
    }

    public function resetQuery(){
        $this->columnsToGet = array();
    }
}