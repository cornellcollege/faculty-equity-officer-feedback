<?php

// Autoload classes based on a 1:1 mapping from namespace to directory structure.
spl_autoload_register(function ($className){
    $ds = '\\';
    $dir = __DIR__;

    // Replace namespace separator with directory separator.
    $className = str_replace('\\', $ds, $className);

    // Get full name of file containing the required class.
    $file = "{$dir}\\{$className}.php";

    // Get file if it is readable.
    if(is_readable($file)) require_once $file;
});



function autoloadModels($class) {
    $parts = explode('\\', $class);
    if(file_exists('./includes/models/' .end($parts) . '.php')){
        include './includes/models/' .end($parts) . '.php';
    }
}

spl_autoload_register('autoloadModels');



function autoloadComponents($class) {
    $parts = explode('\\', $class);
    if(file_exists('./display/components/' .end($parts) . '.thtml')){
        include './display/components/' . end($parts) . '.thtml';
    }
}

spl_autoload_register('autoloadComponents');



function autoloadTables($class) {
    $parts = explode('\\', $class);
    if(file_exists('./includes/tables/' .end($parts) . '.php')){
        include './includes/tables/' .end($parts) . '.php';
    }
}

spl_autoload_register('autoloadTables');

?>