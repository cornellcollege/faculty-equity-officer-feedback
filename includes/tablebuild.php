 <?php
	// The tables to build or rebuild for testing
	$this->aRebuildForTesting = array('faculty_officer_feedback','faculty_officer_feedback_test');
	$this->aTestTablesToBuild = array('faculty_officer_feedback');

	// The tables to build to support the application
	/* This is a array of arrays the structure is all tables will be built if id, created and modified fields:
		Array('sTableName' => Array(
			'sFieldName'=>'<TYPE>:<NULL>:<DEFAULT>'));
		$aTablesFields =  array('sponsor_username'=>array('type'=>'varchar(50)','null'=>'NULL','default'=>''),
								'sponsored_group_or_ind'=>array('type'=>'varchar(250)','null'=>'NULL','default'=>''),
								'closed'=>array('type'=>'tinyint(1)','null'=>'NOT NULL','default'=>0)
							   );
		$aTablesFields = array(
								   ''=>array(
											''=>array(
														'type'=>'','null'=>'','default'=>''),
											''=>array(
														'type'=>'','null'=>'','default'=>''),
											''=>array(
														'type'=>'','null'=>'','default'=>''),
											),
								   );
			
	*/
	// "files" table used by file_upload function
	// "file_log" table used by saveFiles function
	// id, created and modified are add during the table build you do not need to include these fields
	$this->aTablesFields = array(
								   'faculty_officer_feedback'=>array(
											'feedback_text'=>array(
														'type'=>'mediumtext','null'=>'','default'=>''),
											'email'=>array(
														'type'=>'varchar(100)','null'=>'','default'=>''),
											'contact'=>array(
														'type'=>'tinyint(1)','null'=>'NOT NULL','default'=>'1')
											),
								);
	$this->aTablesFields = array(
									'faculty_officer_feedback_test'=>array(
											 'feedback_text'=>array(
														 'type'=>'mediumtext','null'=>'','default'=>''),
											 'email'=>array(
														 'type'=>'varchar(100)','null'=>'','default'=>''),
											 'contact'=>array(
														 'type'=>'tinyint(1)','null'=>'NOT NULL','default'=>'1')
											 ),
								 );
?>
