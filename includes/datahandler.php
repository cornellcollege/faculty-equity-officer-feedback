<?php
namespace CLib;
// We will want to be able to use the PHP DateTime class so we need to be able to call it from within the CLib namespace.
use \DateTime;
use \DateInterval;
use \SplFileInfo;
use CLib\Database\PowerCampus;

/**
 * DataHandler class is the M portion of the MVC
 * @property SQL $sql if using either mysql (being retired) or mysqlPDO
 * @property SQL $sungard if using mssql
 * @property class UserInfo auto loaded into $this->info
 * @property function insert() inserts the given array into the named table
 * @property function query() runs the given query
 * @property object DateInterval from the PHP namespace
 * @property object DateTime from the PHP namespace
 */
class CornellDataHandler  extends Datahandler{
    // Variables used in the class
	public $aSelectChoices = array( 1 => 'No', 2 => 'Yes');

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////// Functions for this applications specific use ///////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Class constructor
     * @param Object $fw The Framework object
     * @param Object $sql The PDO object for the local database
     * @param Object $dynamicPC The MSSql object for PowerCampus
     * @param Object $dynamicGP The MSSql object for GreatPlains
     * @return Object The Datahandler
    */
	public function __construct($fw, $sql, $config, $dynamicPC = null, $dynamicGP = null, $bReadInTables = true, $bReadFromIncludes = true, $dynamicPF = null){
		parent::__construct($fw, $sql, $config, $dynamicPC, $dynamicGP, $bReadInTables, $bReadFromIncludes, $dynamicPF);
        $sShortUri = substr($this->uri, 0, (strpos($this->uri, '/index.php')));
		$this->file_root = 'W:/html/public'.$sShortUri.'/files';
	}
	
	/**
     * class initialization function
     * Add in processing and application settings here.
     * One of the practices is to run the parent init to make sure the base Datahandler is initialized correctly
     * @param NONE
    */
    public function init(){
		parent::init($config);
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// Get information below here //////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * aGetAdminInfo used to pull all the data from the faculty_officer_feedback table to
     * display on the admin page.
     * @param NONE
     * @return array all the active records in the faculty_officer_feedback table
    */
    public function getFeedback(){
        // initialize the return variable
        $aReturn = array();
		// Set the table name to pull from
		$tableName1 = 'faculty_officer_feedback';
		// If in testing mode use the testing table
		if($this->config['testing']){
			$tableName1 .= '_test';
		}
		// Build the query
		$sQuery = "
			SELECT * FROM ".$tableName1." order by id DESC;
		";
		// Run the query
        $results = $this->sql->query($sQuery);
        return $results;
    }
	/**
	 * bCheckIfInfoChanged Is used to test if any of the input fields changed during the process
	 * @param Array $aOldData The old data used as the baseline
	 * @param Array $aNewData The new data from the currently submitted form
	 * @return Boolean True if they are different else False
	*/
	public function bCheckIfInfoChanged($aOldData,$aNewData){
		$bReturn = false;
		foreach($aNewData as $key=>$val){
			if(array_key_exists( $key , $aOldData )){
				if($aOldData[$key] != $aNewData[$key]){
					$bReturn = true;
				}
			}
		}
		return $bReturn;
	}
	
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////// insert / update information below here /////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 * 
	 * 
	 */
	public function insertEquityOfficerFeedback($tableData){
		$tableName = 'faculty_officer_feedback';
		if($this->fw->isTesting){
			$tableName .= '_test';
		}
		$insertData = array();
		$insertData['feedback_text'] = $tableData['feedback_text'];
		$insertData['contact'] = $tableData['contact'];
		$insertData['email'] = $tableData['email'];
		$result = $this->sql->insert($tableName,$insertData);
		if($result){
			$message = "A new Equity Officer Feedback form was submitted.<br/>".
			"<b>Feedback Given:</b><br/>".
			$insertData['feedback_text']."<br/>".
			"<b>E-Mail (if given):</b><br/> ".$insertData['email']."<br/>";
		}else{
			$message = null;
		}
		// Return the message to the calling process
		return $message;
	}
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////// Example of AJAX called within the application //////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * check_list_sponsers is a call to query the files table
     * @param String $username The username
     * Example of the query that the ajax query would run.
     * Will error out unless you connect to the proper DB
    */
    public function check_list_sponsers($username){
        $files_result = $this->sql->fetch('files', array(), "username='{$username}'");
        foreach($files_result as $files_record){
            if(!empty($files_record['uploaded'])) {
                $time = $files_record['uploaded'];
                $myVal = true;
            } else {
                $time = '0000-00-00 00:00:00';
                $myVal = false;
            }
            $results[] = array(
                'name' => $files_record['name'],
                'value' => $myVal,
                'time' => $time,
            );
        }
        return $results;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////   Standard Functions ///////////////////////////////////////////////	
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * aCleanArrayIn function is used to substitute special characters in the
	 * input array with their ASCII value surrounded by pipes.
	 * @param Array $aConvertArray The array of information to convert
	 * @return Array The array with the data substituted in
	*/
	public function aCleanArrayIn($aConvertArray){
		foreach($aConvertArray as $key=>$val){
			if(($key <> 'created') && ($key <> 'updated') && ($key <> 'sort_target_date') && ($key <> 'multi_team') && ($key <> 'other_groups')){
				$val = str_replace('|60|','',$val);
				$val = str_replace('|62|','',$val);
				$val = str_replace('!', '|33|',$val);
				$val = str_replace('"', '|34|',$val);
				$val = str_replace('#', '|35|',$val);
				$val = str_replace('$', '|36|',$val);
				$val = str_replace('%', '|37|',$val);
				$val = str_replace('&', '|38|',$val);
				$val = str_replace("'", '|39|',$val);
				$val = str_replace('(', '|40|',$val);
				$val = str_replace(')', '|41|',$val);
				$val = str_replace('*', '|42|',$val);
				$val = str_replace('+', '|43|',$val);
				$val = str_replace(',', '|44|',$val);
				$val = str_replace('-', '|45|',$val);
				$val = str_replace('.', '|46|',$val);
				$val = str_replace('/', '|47|',$val);
				$val = str_replace(':', '|58|',$val);
				$val = str_replace(';', '|59|',$val);
				$val = str_replace('<', '|60|',$val);
				$val = str_replace('=', '|61|',$val);
				$val = str_replace('>', '|62|',$val);
				$val = str_replace('?', '|63|',$val);
				$val = str_replace('@', '|64|',$val);
				$val = str_replace(']', '|91|',$val);
				$val = str_replace('\\', '|92|',$val);
				$val = str_replace('[', '|93|',$val);
				$val = str_replace('{', '|123|',$val);
				$val = str_replace('}', '|125|',$val);
				$val = str_replace('~', '|126|',$val);
			}
			$aConvertArray[$key]=$val;
		}
		return $aConvertArray;
	}

	/**
	 * aCleanArrayOut function is used to substitute ASCII value surrounded by pipes in the
	 * input array with their special characters value.
	 * @param Array $aConvertArray The array of information to convert
	 * @return Array The array with the data substituted in
	*/
	public function aCleanArrayOut($aConvertArray){
		foreach($aConvertArray as $key=>$val){
			if(($key <> 'created') && ($key <> 'updated') && ($key <> 'sort_target_date') && ($key <> 'multi_team') && ($key <> 'other_groups')){
				$val = str_replace('|33|', '!',$val);
				$val = str_replace('|34|', '"',$val);
				$val = str_replace('|35|', '#',$val);
				$val = str_replace('|36|', '$',$val);
				$val = str_replace('|37|', '%',$val);
				$val = str_replace('|38|', '&',$val);
				$val = str_replace('|39|', "'",$val);
				$val = str_replace('|40|', '(',$val);
				$val = str_replace('|41|', ')',$val);
				$val = str_replace('|42|', '*',$val);
				$val = str_replace('|43|', '+',$val);
				$val = str_replace('|44|', ',',$val);
				$val = str_replace('|45|', '-',$val);
				$val = str_replace('|46|', '.',$val);
				$val = str_replace('|47|', '/',$val);
				$val = str_replace('|58|', ':',$val);
				$val = str_replace('|59|', ';',$val);
				$val = str_replace('|60|', '<',$val);
				$val = str_replace('|61|', '=',$val);
				$val = str_replace('|62|', '>',$val);
				$val = str_replace('|63|', '?',$val);
				$val = str_replace('|64|', '@',$val);
				$val = str_replace('|91|', ']',$val);
				$val = str_replace('|92|', '\\',$val);
				$val = str_replace('|93|', '[',$val);
				$val = str_replace('|123|', '{',$val);
				$val = str_replace('|125|', '}',$val);
				$val = str_replace('|126|', '~',$val);
			}
			$aConvertArray[$key]=$val;
		}
		return $aConvertArray;
	}

    /**
     * bEmailForm($sFormName) is used to email the form request to the distribution list in the email table
     * @param String, The nema of the form to email
     * @return Boolean, True on success false on failure
     * @example $this->bEmailForm($sEmailForm, $iInsertedID, $sEmailAction, $sEmailAddress);
    */
//	public function bEmailForm($sFormName, $iRecordNum, $sAction = null, $sEmailAddress = null, $sEmailSubject = null){
	public function bEmailForm($sFormName){
        $sSubject = "";
        $body = file_get_contents('display/email/'.$sFormName.'.thtml');
		$aEmailList = array();
		$sQuery = 'select * from email_recpt where active = 1 and form_name = "'.$sFormName.'";';
        $results = $this->sql->query($sQuery);
        if(!empty($results)){
            foreach($results as $record){
                $aEmailList[] = $record['email_address'];
            }
        }
		$sMessage = '<table>';
		$sEmailAttachment = '';
		$iCount = 2;
		foreach($this->aMailData as $key=>$val){
			if($key == 'file_attachment'){
				$sEmailAttachment = '<a href="' . $val . '">Attached file: ' . $val . '</a>.';
			} else{
				if(strlen(trim($val)) > 0){
					if(($iCount % 2) == 0){
						$sMessage .= '<tr style="background:#f1e5fc">';
					} else {
						$sMessage .= '<tr style="background:#dcc0f7">';
					}
					$sMessage .= '<td width="550" padding="0.5em 1em 0.5em 0.5em" vertical-align="top" border="1px solid #888888" line-height="1.5">';
					$sMessage .= $val;
					$sMessage .= '</td></tr>';
					$iCount ++;
				}
			}
		}
		// passed in from the framework: $bEmailSuccess = $this->data->bEmailForm('index_email');
		switch ($sFormName) {
			case "index_email":
				$sSubject = "Notification: Index Page Email Subject";
				break;
			case "second_email_choice":
				$sSubject = "Notification: Second Email Choice Subject";
				break;
		}
        $find = array(
                    '[BODYTABLE]',
                    '[ATTACHMENT]',
                    );
        $replace = array(
                    $sMessage,
                    $sEmailAttachment,
                    );
        $body1 = str_replace($find, $replace, $body);
		foreach($aEmailList as $key=>$val){
			$this->fw->sendEmail($sSubject, $body1, $val, 'no-reply@cornellcollege.edu', true);
		}
        // we made it through so return true
        return true;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////
	
}
