<?php
	// Fields to exclude from the email
	$this->keywords = array('submit','closed','username','student_id','active');
	// Field replacement titles to use in the email
	$this->aFieldsTitles = array(
									'student_name'=>'Name: ',
									'student_first_name'=>'First name: ',
									'student_last_name'=>'Last name: ',
									'student_email'=>'Student email: ',
									'student_sig'=>'Student signature: ',
									'class_standing'=>'Current class standing: ',
									'relationship_to_college'=>'Relationship to the college: ',
									'requester_name'=>'Requester name: ',
									'requester_phone_number'=>'Contact phone number: ',
									'requester_email'=>'Contact email: ',
									'requester_purpose'=>'Request purpose: ',
									'requester_need_date'=>'Needed by: ',
									'requester_previously'=>'Previously requested? ',
									'requester_date_range'=>'Retrieve information for: ',
									'requester_date_range_rationale'=>'Date range rationale: ',
									'requester_fields'=>'Requested fields: ',
									'requester_fields_other'=>'Other fields requested: ',
									'requester_specifications'=>'Requested specifications: ',
									'requester_sort_order'=>'Sort order: ',
									'requester_sort_order_specifications'=>'Sort order specifications: '
								   );
?>
