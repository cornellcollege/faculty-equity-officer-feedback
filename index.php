<?php
// Set the namespace for the application CLib is the standard Cornell Library 
namespace CLib;

//Setup core includes
// Read in the auto load code.  This will automatically read in classes and their functions within the namespace
//  just by calling them no need to us an require_once or include
require_once 'autoload.php';
// Read in the applications configuration file
require_once 'includes/config.php';
// Read in classes from the app.
require_once 'includes/autoload.php';
//require_once 'w:\html\app_config\<ProjectName>\<UpdateDate>.php';
// Read in the application specific framework file
require_once 'includes/framework.php';

// Create a new instance of the CornellFramework (all the core functionality)
$fw = new CornellFramework();
// Initialize the framework
$fw->init($config);
// Rout the requested functionality through the framework
$fw->router();
// Lastly call the Framework to render the page
$fw->render();
